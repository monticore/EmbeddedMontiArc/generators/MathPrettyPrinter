<#-- (c) https://github.com/MontiCore/monticore -->
        var radios = document.querySelectorAll("input[name='View']");
        var card = document.querySelector(".card");
        var front = document.querySelectorAll(".card-face-front tr");
        var back = document.querySelectorAll(".card-face-back tr");

        function clickButton(index) {
            radios[index].click();
        }

        function selectRange(start, end) {
            var length = front.length;

            for(var i = 0; i < length; i++) {
                if(i >= start - 1 && i < (end || start)) {
                    front[i].classList.add("marked");
                    back[i].classList.add("marked");
                } else {
                    front[i].classList.remove("marked");
                    back[i].classList.remove("marked");
                }
            }
        }

        function scroll(index, start) {
            start = start - 1;

            window.scrollTo(0, index === 0 ? front[start].offsetTop : back[start].offsetTop);
        }

        function onChange(event) {
            card.classList.toggle("flipped");
        }

        function onHashChange() {
            window.setTimeout(function() {
                var hash = window.location.hash;
                var parts = hash.substring(1).split(':');

                var index = parts[0];
                var start = parts[1];
                var end = parts[2];

                if(index) {
                    clickButton(index);

                    if(start) {
                        scroll(index, start);
                        selectRange(start, end);
                    }
                }
            }, 0);
        }

        function onWindowResize() {

        }

        radios.forEach(function(element) { element.addEventListener("change", onChange) });
        onHashChange();
        window.addEventListener("hashchange", onHashChange);
        window.addEventListener("resize", onWindowResize);

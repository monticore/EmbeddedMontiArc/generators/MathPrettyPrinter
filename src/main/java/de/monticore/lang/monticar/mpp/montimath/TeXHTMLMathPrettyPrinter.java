/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.mpp.montimath;

import de.monticore.lang.math._ast.*;
import de.monticore.lang.monticar.mpp.core.tex.SVGGenerator;
import de.se_rwth.commons.logging.Log;

import java.io.IOException;

public class TeXHTMLMathPrettyPrinter extends HTMLMathPrettyPrinter implements TeXHTMLMathStructuredVisitor {
    protected final TeXMathPrettyPrinter formatter;
    protected final SVGGenerator generator;

    public TeXHTMLMathPrettyPrinter(SVGGenerator generator) {
        this.formatter = new TeXMathPrettyPrinter();
        this.generator = generator;
    }

    protected void printImageTag(String image) {
        String printedTag = String.format("<img src=\"%s\"></img>", image);

        this.printer.print(printedTag);
    }

    protected void printImageTag(ASTMathNode node) {
        try {
            String nodeLatex = this.formatter.prettyPrint(node);
            String filename = this.generator.generate(nodeLatex);

            this.printImageTag(filename);
        } catch(IOException exception) {
            Log.error(exception.getMessage());
        }
    }

    @Override
    public String prettyPrint(ASTMathCompilationUnit node) {
        this.generator.setContext(node);

        return super.prettyPrint(node);
    }

    @Override
    public void revisit(ASTMathAssignmentDeclarationStatement node) {
        this.printImageTag(node);
    }

    @Override
    public void endVisit(ASTMathDeclarationStatement node) {
        this.printImageTag(node);
        this.printer.println();
    }

    @Override
    public void endVisit(ASTMathAssignmentDeclarationStatement node) {
        this.printer.println();
    }

    @Override
    public void visit(ASTMathAssignmentStatement node) {
        this.printImageTag(node);
    }

    @Override
    public void endVisit(ASTMathAssignmentStatement node) {
        this.printer.println();
    }

    @Override
    public void visit(ASTMathForLoopHead node) {
        this.printFor();
        this.printSpace();
        this.printImageTag(node);
    }

    @Override
    public void visit(ASTMathIfExpression node) {
        this.printIf();
        this.printSpace();
        this.printImageTag(node);
    }

    @Override
    public void visit(ASTMathElseIfExpression node) {
        this.printElseIf();
        this.printSpace();
        this.printImageTag(node);
    }
}

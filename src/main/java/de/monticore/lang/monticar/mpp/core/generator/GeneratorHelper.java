/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.mpp.core.generator;

import com.google.common.eventbus.Subscribe;
import de.monticore.lang.math._ast.ASTMathCompilationUnit;
import de.monticore.lang.math._ast.ASTMathScript;
import de.monticore.lang.monticar.mpp.core.events.EventsService;
import de.monticore.lang.monticar.mpp.core.options.OptionsParsedEvent;
import de.monticore.lang.monticar.mpp.core.options.OptionsService;
import de.monticore.lang.monticar.mpp.core.parser.MathFileParsedEvent;
import de.monticore.lang.monticar.mpp.core.tex.SVGGenerator;
import de.monticore.lang.monticar.mpp.montimath.HTMLMathPrettyPrinter;
import de.monticore.lang.monticar.mpp.montimath.TeXHTMLMathPrettyPrinter;
import de.monticore.types.types._ast.ASTQualifiedName;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GeneratorHelper {
    protected final EventsService eventsService;
    protected final OptionsService optionsService;
    protected final HTMLMathPrettyPrinter htmlPrinter;
    protected final TeXHTMLMathPrettyPrinter texPrinter;
    protected final SVGGenerator svggenerator;

    protected List<ASTMathCompilationUnit> nodes;

    public GeneratorHelper(EventsService eventsService, OptionsService optionsService) {
        this.eventsService = eventsService;
        this.optionsService = optionsService;

        this.svggenerator = new SVGGenerator(this.optionsService);
        this.htmlPrinter = new HTMLMathPrettyPrinter();
        this.texPrinter = new TeXHTMLMathPrettyPrinter(this.svggenerator);

        this.nodes = new ArrayList<>();

        this.eventsService.register(this);
    }

    public List<ASTMathCompilationUnit> getASTNodes() {
        return this.nodes;
    }

    public String printPath(ASTMathCompilationUnit ast) {
        ASTQualifiedName packageNode = ast.getPackage();
        String packageName = packageNode.toString().replace(".", "/");
        String scriptName = this.printScript(ast);

        return String.format("%s/%s/index.html", packageName, scriptName);
    }

    public String printScript(ASTMathCompilationUnit ast) {
        ASTMathScript scriptNode = ast.getMathScript();

        return scriptNode.getName();
    }

    public String printHTML(ASTMathCompilationUnit ast) {
        return this.htmlPrinter.prettyPrint(ast);
    }

    public String printTeXHTML(ASTMathCompilationUnit ast) {
        return this.texPrinter.prettyPrint(ast);
    }

    public String[] getHTMLLines(ASTMathCompilationUnit ast) {
        String printedHTML = this.printHTML(ast);

        return printedHTML.split("\n");
    }

    public String[] getTeXHTMLLines(ASTMathCompilationUnit ast) {
        String printedHTML = this.printTeXHTML(ast);

        return printedHTML.split("\n");
    }


    @Subscribe
    public void onMathFileParsed(MathFileParsedEvent event) {
        this.nodes.add(event.ast);
    }

    @Subscribe
    public void onOptionsParsed(OptionsParsedEvent event) {
        boolean isInternal = this.optionsService.isInternal();

        this.texPrinter.setInternal(isInternal);
        this.htmlPrinter.setInternal(isInternal);
    }
}

/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.mpp.montimath;

import de.monticore.assignmentexpressions._ast.ASTMinusPrefixExpression;
import de.monticore.ast.Comment;
import de.monticore.commonexpressions._ast.*;
import de.monticore.lang.math._ast.*;
import de.monticore.lang.matrix._ast.ASTMathMatrixAccessExpression;
import de.monticore.lang.matrix._ast.ASTMathMatrixValueExplicitExpression;
import de.monticore.lang.matrix._ast.ASTMathVectorExpression;
import de.monticore.lang.matrixexpressions._ast.*;
import de.monticore.lang.monticar.ranges._ast.ASTRange;
import de.monticore.lang.monticar.types2._ast.ASTDimension;
import de.monticore.lang.monticar.types2._ast.ASTElementType;
import de.monticore.literals.literals._ast.ASTDoubleLiteral;
import de.monticore.literals.literals._ast.ASTIntLiteral;
import de.monticore.literals.literals._ast.ASTSignedIntLiteral;
import de.monticore.numberunit._ast.ASTNumberWithInf;
import de.monticore.numberunit._ast.ASTSIUnitBasic;
import de.monticore.numberunit._ast.ASTTimeDiv;
import de.monticore.numberunit._ast.ASTUnitBaseDimWithPrefix;
import de.monticore.prettyprint.AstPrettyPrinter;
import de.monticore.prettyprint.IndentPrinter;
import de.monticore.types.types._ast.ASTImportStatement;
import de.monticore.types.types._ast.ASTQualifiedName;

import java.util.List;

public class MathPrettyPrinter implements AstPrettyPrinter<ASTMathCompilationUnit>, MathStructuredVisitor {
    protected final IndentPrinter printer;
    protected boolean importVisited;
    protected boolean inMatrixEnvironment;
    protected boolean internal;

    public MathPrettyPrinter(int spacing, boolean internal) {
        this.printer = new IndentPrinter();
        this.printer.setIndentLength(spacing);
        this.importVisited = false;
        this.inMatrixEnvironment = false;
        this.internal = internal;
    }

    public MathPrettyPrinter(int spacing) {
        this(spacing, false);
    }

    public MathPrettyPrinter() {
        this(4,false);
    }

    public void setInternal(boolean internal) {
        this.internal = internal;
    }

    @Override
    public String prettyPrint(ASTMathCompilationUnit node) {
        this.printer.clearBuffer();

        if(this.internal) {
            this.printStatements(node);
        } else {
            this.handle(node);
        }

        return this.printer.getContent();
    }

    public String prettyPrint(ASTMathNode node) {
        this.printer.clearBuffer();
        node.accept(this);

        return this.printer.getContent();
    }

    protected void printStatements(ASTMathCompilationUnit node) {
        List<ASTStatement> statements = node.getMathScript().getStatementsList();

        for(ASTStatement statement: statements)
        {
            statement.accept(this);
        }
    }

    protected void printSpace() {
        this.printer.print(" ");
    }

    protected void printComments(List<Comment> comments) {
        for (Comment comment : comments) {
            String text = comment.getText();

            this.printComment(text);
        }
    }

    protected void printComment(String comment) {
        String[] lines = comment.split("\n");

        if (lines.length > 0) lines[0] = lines[0].trim();

        for (String line : lines) {
            this.printer.println(line);
        }
    }

    /*========================================================
    == TOKENS ================================================
    ========================================================*/
    protected void printPackage() {
        this.printer.print("package");
    }

    protected void printSemicolon() {
        this.printer.print(";");
    }

    protected void printScript() {
        this.printer.print("script");
    }

    protected void printImport() {
        this.printer.print("import");
    }

    protected void printEnd() {
        this.printer.print("end");
    }

    protected void printColon() {
        this.printer.print(":");
    }

    protected void printFor() {
        this.printer.print("for");
    }

    protected void printIf() {
        this.printer.print("if");
    }

    protected void printElseIf() {
        this.printer.print("elseif");
    }

    protected void printElse() {
        this.printer.print("else");
    }

    protected void printCircumFlex() {
        this.printer.print("^");
    }

    protected void printLeftCurlyBracket() {
        this.printer.print("{");
    }

    protected void printRightCurlyBracket() {
        this.printer.print("}");
    }

    protected void printComma() {
        this.printer.print(",");
    }

    protected void printLeftSquareBracket() {
        this.printer.print("[");
    }

    protected void printRightSquareBracket() {
        this.printer.print("]");
    }

    protected void printLeftBracket() {
        this.printer.print("(");
    }

    protected void printRightBracket() {
        this.printer.print(")");
    }

    /*========================================================
    == VISITOR ===============================================
    ========================================================*/
    @Override
    public void visit(ASTMathCompilationUnit node) {
        this.printComments(node.get_PreCommentList());
        this.printPackage();
        this.printSpace();
    }

    @Override
    public void revisit(ASTMathCompilationUnit node) {
        this.printSemicolon();
        this.printer.println();
    }

    @Override
    public void visit(ASTQualifiedName node) {
        this.printer.print(node.toString());
    }

    @Override
    public void visit(ASTMathScript node) {
        this.printer.println();
        this.printComments(node.get_PreCommentList());
        this.printScript();
        this.printSpace();
        this.printer.print(node.getName());
        this.printer.println();
        this.printer.indent();
    }

    @Override
    public void endVisit(ASTMathScript node) {
        this.printer.unindent();
        this.printEnd();
    }

    @Override
    public void visit(ASTImportStatement node) {
        String printedImport = String.join(".", node.getImportList());

        printedImport += (node.isStar() ? ".*" : "");

        if (!this.importVisited) {
            this.printer.println();
            this.importVisited = true;
        }

        this.printImport();
        this.printSpace();
        this.printer.print(printedImport);
    }

    @Override
    public void endVisit(ASTImportStatement node) {
        this.printSemicolon();
        this.printer.println();
    }

    @Override
    public void visit(ASTMathAssignmentDeclarationStatement node) {
        this.printComments(node.get_PreCommentList());
    }

    @Override
    public void revisit(ASTMathAssignmentDeclarationStatement node) {
        this.printSpace();
        this.printer.print(node.getName());
    }

    @Override
    public void endVisit(ASTMathAssignmentDeclarationStatement node) {
        this.printSemicolon();
        this.printer.println();
    }

    @Override
    public void visit(ASTElementType node) {
        this.printer.print(node.getName());
    }

    @Override
    public void visit(ASTMathAssignmentOperator node) {
        this.printSpace();
        this.printer.print(node.getOperator());
        this.printSpace();
    }

    @Override
    public void visit(ASTIntLiteral node) {
        this.printer.print(node.getValue());
    }

    @Override
    public void visit(ASTMathAssignmentStatement node) {
        this.printComments(node.get_PreCommentList());

        if (node.isPresentName()) this.printer.print(node.getName());
    }

    @Override
    public void endVisit(ASTMathAssignmentStatement node) {
        this.printSemicolon();
        this.printer.println();
    }

    @Override
    public void revisit(ASTMathVectorExpression node) {
        this.printColon();
    }

    @Override
    public void visit(ASTNameExpression node) {
        this.printer.print(node.getName());
    }

    @Override
    public void visit(ASTMathForLoopHead node) {
        this.printFor();
        this.printSpace();
        this.printer.print(node.getName());
        this.printSpace();
        this.printer.print("=");
        this.printSpace();
    }

    @Override
    public void endVisit(ASTMathForLoopHead node) {
        this.printer.println();
        this.printer.indent();
    }

    public void visit(ASTMathForLoopExpression node) {
        this.printComments(node.get_PreCommentList());
    }

    @Override
    public void endVisit(ASTMathForLoopExpression node) {
        this.printer.unindent();
        this.printEnd();
        this.printer.println();
    }

    @Override
    public void visit(ASTMathIfExpression node) {
        this.printIf();
        this.printSpace();
    }

    public void revisit(ASTMathIfExpression node) {
        this.printer.println();
        this.printer.indent();
    }

    @Override
    public void endVisit(ASTMathIfExpression node) {
        this.printer.unindent();
    }

    @Override
    public void endVisit(ASTMathIfStatement node) {
        this.printEnd();
        this.printer.println();
    }

    @Override
    public void visit(ASTMultExpression node) {
        this.printSpace();
        this.printer.print(node.getOperator());
        this.printSpace();
    }

    @Override
    public void revisit(ASTLessThanExpression node) {
        this.printSpace();
        this.printer.print(node.getOperator());
        this.printSpace();
    }

    /*public void visit(ASTMathStatements node) {
        this.printer.println();
        this.printer.indent();
        this.printComments(node.get_PreCommentList());
    }

    @Override
    public void endVisit(ASTMathStatements node) {
        this.printComments(node.get_PostCommentList());
        this.printer.unindent();
    }*/

    @Override
    public void revisit(ASTLessEqualExpression node) {
        this.printSpace();
        this.printer.print(node.getOperator());
        this.printSpace();
    }

    @Override
    public void revisit(ASTGreaterThanExpression node) {
        this.printSpace();
        this.printer.print(node.getOperator());
        this.printSpace();
    }

    @Override
    public void visit(ASTMathElseIfExpression node) {
        this.printElseIf();
        this.printSpace();
    }

    @Override
    public void revisit(ASTMathElseIfExpression node) {
        this.printer.println();
        this.printer.indent();
    }

    @Override
    public void endVisit(ASTMathElseIfExpression node) {
        this.printer.unindent();
    }

    @Override
    public void visit(ASTMathElseExpression node) {
        this.printElse();
        this.printer.println();
        this.printer.indent();
    }

    @Override
    public void endVisit(ASTMathElseExpression node) {
        this.printer.unindent();
    }

    @Override
    public void visit(ASTMinusExpression node) {
        this.printSpace();
        this.printer.print(node.getOperator());
        this.printSpace();
    }

    @Override
    public void visit(ASTBracketExpression node) {
        this.printLeftBracket();
    }

    @Override
    public void endVisit(ASTBracketExpression node) {
        this.printRightBracket();
    }

    @Override
    public void visit(ASTMathDeclarationStatement node) {
        this.printComments(node.get_PreCommentList());
    }

    @Override
    public void endVisit(ASTMathDeclarationStatement node) {
        this.printSpace();
        this.printer.print(node.getName());
        this.printSemicolon();
        this.printer.println();
    }

    @Override
    public void visit(ASTPlusExpression node) {
        this.printSpace();
        this.printer.print(node.getOperator());
        this.printSpace();
    }

    @Override
    public void visit(ASTDimension node) {
        this.printCircumFlex();
        this.printLeftCurlyBracket();
    }

    @Override
    public void revisit(ASTDimension node) {
        this.printComma();
        this.printSpace();
    }

    @Override
    public void endVisit(ASTDimension node) {
        this.printRightCurlyBracket();
    }

    @Override
    public void visit(ASTMathMatrixValueExplicitExpression node) {
        this.inMatrixEnvironment = true;
        this.printLeftSquareBracket();
    }

    @Override
    public void revisit(ASTMathMatrixValueExplicitExpression node) {
        this.printSemicolon();
        this.printSpace();
    }

    @Override
    public void endVisit(ASTMathMatrixValueExplicitExpression node) {
        this.printRightSquareBracket();
        this.inMatrixEnvironment = false;
    }

    @Override
    public void revisit(ASTMathMatrixAccessExpression node) {
        this.printComma();
        this.printSpace();
    }

    @Override
    public void visit(ASTUnitBaseDimWithPrefix node) {
        this.printSpace();
        this.printer.print(node.getName());
    }

    @Override
    public void visit(ASTRange node) {
        this.printLeftBracket();
    }

    @Override
    public void revisit(ASTRange node) {
        this.printSpace();
        this.printColon();
        this.printSpace();
    }

    @Override
    public void endVisit(ASTRange node) {
        this.printRightBracket();
    }

    @Override
    public void visit(ASTSignedIntLiteral node) {
        this.printer.print(node.getValue());
    }

    @Override
    public void revisit(ASTSIUnitBasic node) {
        if (node.isPresentSignedIntLiteral()) this.printCircumFlex();
    }

    @Override
    public void visit(ASTTimeDiv node) {
        if (node.isPresentIsDiv()) {
            this.printSpace();
            this.printer.print(node.getIsDiv());
        }

        if (node.isPresentIsTime()) {
            this.printSpace();
            this.printer.print(node.getIsTime());
        }
    }

    @Override
    public void visit(ASTMathMatrixNameExpression node) {
        this.printer.print(node.getName());
    }

    @Override
    public void visit(ASTMathMatrixAccessExpression node) {
        if (!this.inMatrixEnvironment) this.printLeftBracket();
    }

    @Override
    public void endVisit(ASTMathMatrixAccessExpression node) {
        if (!this.inMatrixEnvironment) this.printRightBracket();
    }

    @Override
    public void revisit(ASTEqualsExpression node) {
        this.printSpace();
        this.printer.print(node.getOperator());
        this.printSpace();
    }

    @Override
    public void revisit(ASTNotEqualsExpression node) {
        this.printSpace();
        this.printer.print(node.getOperator());
        this.printSpace();
    }

    @Override
    public void revisit(ASTGreaterEqualExpression node) {
        this.printSpace();
        this.printer.print(node.getOperator());
        this.printSpace();
    }

    @Override
    public void visit(ASTNumberWithInf node) {
        if (node.isPresentNegNumber()) this.printer.print(node.getNegNumber());
        if (node.isPresentNegInf()) this.printer.print(node.getNegInf());
        if (node.isPresentPosInf()) this.printer.print(node.getPosInf());
    }

    @Override
    public void revisit(ASTBooleanOrOpExpression node) {
        this.printSpace();
        this.printer.print(node.getOperator());
        this.printSpace();
    }

    @Override
    public void revisit(ASTBooleanAndOpExpression node) {
        this.printSpace();
        this.printer.print(node.getOperator());
        this.printSpace();
    }

    @Override
    public void revisit(ASTMathArithmeticPowerOfExpression node) {
        this.printCircumFlex();
    }

    @Override
    public void revisit(ASTModuloExpression node) {
        this.printSpace();
        this.printer.print(node.getOperator());
        this.printSpace();
    }

    @Override
    public void revisit(ASTDivideExpression node) {
        this.printSpace();
        this.printer.print(node.getOperator());
        this.printSpace();
    }

    @Override
    public void endVisit(ASTMathArithmeticMatrixComplexTransposeExpression node) {
        this.printer.print("'");
    }

    @Override
    public void revisit(ASTMathArithmeticMatrixEEPowExpression node) {
        this.printer.print(".^");
    }

    @Override
    public void revisit(ASTMathArithmeticMatrixLeftDivideExpression node) {
        this.printSpace();
        this.printer.print("\\");
        this.printSpace();
    }

    @Override
    public void revisit(ASTMathArithmeticMatrixEEMultExpression node) {
        this.printSpace();
        this.printer.print(".*");
        this.printSpace();
    }

    @Override
    public void revisit(ASTMathArithmeticMatrixEERightDivideExpression node) {
        this.printSpace();
        this.printer.print("./");
        this.printSpace();
    }

    @Override
    public void revisit(ASTMathArithmeticMatrixEELeftDivideExpression node) {
        this.printSpace();
        this.printer.print(".\\");
        this.printSpace();
    }

    @Override
    public void visit(ASTDoubleLiteral node) {
        this.printer.print(node.getValue());
    }

    @Override
    public void visit(ASTMinusPrefixExpression node) {
        this.printer.print("-");
    }
}

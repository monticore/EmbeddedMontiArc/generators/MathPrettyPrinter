/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.mpp.montimath;

import de.monticore.ast.ASTNode;
import de.monticore.commonexpressions._ast.*;
import de.monticore.lang.math._ast.*;
import de.monticore.lang.matrix._ast.ASTMathMatrixAccessExpression;
import de.monticore.lang.matrix._ast.ASTMathMatrixValueExplicitExpression;
import de.monticore.lang.matrixexpressions._ast.ASTMathArithmeticMatrixComplexTransposeExpression;
import de.monticore.lang.matrixexpressions._ast.ASTMathArithmeticMatrixEEPowExpression;
import de.monticore.lang.monticar.types2._ast.ASTDimension;
import de.monticore.lang.monticar.types2._ast.ASTElementType;
import de.monticore.lang.matrixexpressions._ast.*;
import de.monticore.numberunit._ast.ASTTimeDiv;
import de.monticore.numberunit._ast.ASTUnitBaseDimWithPrefix;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class TeXMathPrettyPrinter extends MathPrettyPrinter implements TeXMathStructuredVisitor {
    protected Stack<Boolean> nextParentheses;
    protected Set<ASTNode> printedParentheses;

    public TeXMathPrettyPrinter() {
        this.printedParentheses = new HashSet<>();
        this.nextParentheses = new Stack<>();
    }

    @Override
    public void visit(ASTElementType node) {
        String printedNode = String.format("\\mathbb{%s}", node.getName());

        this.printer.print(printedNode);
    }

    @Override
    public void endVisit(ASTElementType node) {
        this.printer.print("\\;");
    }

    @Override
    public void revisit(ASTDimension ast) {
        this.printSpace();
        this.printer.print("\\times");
        this.printSpace();
    }

    @Override
    public void visit(ASTMultExpression node) {
        this.printSpace();
        this.printer.print("\\cdot");
        this.printSpace();
    }

    @Override
    public void visit(ASTDivideExpression node) {
        this.printer.print("\\dfrac{");
        this.nextParentheses.push(false);
    }

    @Override
    public void revisit(ASTDivideExpression node) {
        this.printer.print("}{");
    }

    @Override
    public void endVisit(ASTDivideExpression node) {
        this.printer.print("}");
        this.nextParentheses.pop();
    }

    @Override
    public void revisit(ASTMathAssignmentDeclarationStatement node) {
        String printedNode = String.format("\\text{%s}", node.getName());

        //this.printer.print("~");
        this.printer.print(printedNode);
        //this.printer.print("~");
    }

    @Override
    public void visit(ASTMathAssignmentStatement node) {
        if (node.isPresentName()) {
            String printedNode = String.format("\\text{%s}", node.getName());

            this.printer.print(printedNode);
        }

        //this.printer.print("~");
    }

    @Override
    public void visit(ASTMathDeclarationStatement node) {

    }

    @Override
    public void endVisit(ASTMathDeclarationStatement node) {
        //this.printer.print("~");
        this.printer.print(node.getName());
        this.printSemicolon();
        this.printer.println();
    }

    @Override
    public void visit(ASTTimeDiv node) {
        //TODO: Create LaTeX
        if (node.isPresentIsDiv()) {
            this.printer.print("\\;");
            this.printer.print(node.getIsDiv());
        }

        if (node.isPresentIsTime()) {
            this.printer.print("\\;");
            this.printer.print("\\cdot");
        }
    }

    @Override
    public void visit(ASTMathMatrixValueExplicitExpression node) {
        this.inMatrixEnvironment = true;
        this.printer.print("\\begin{bmatrix}");
    }

    @Override
    public void revisit(ASTMathMatrixValueExplicitExpression node) {
        this.printer.print("\\\\\\\\");
    }

    @Override
    public void endVisit(ASTMathMatrixValueExplicitExpression node) {
        this.printer.print("\\end{bmatrix}");
        this.inMatrixEnvironment = false;
    }

    @Override
    public void revisit(ASTMathMatrixAccessExpression node) {
        this.printSpace();

        if (this.inMatrixEnvironment) this.printer.print("&");
        else this.printComma();

        this.printSpace();
    }

    @Override
    public void visit(ASTMathMatrixAccessExpression node) {
        if (!this.inMatrixEnvironment) this.printLeftBracket();
    }

    @Override
    public void endVisit(ASTMathMatrixAccessExpression node) {
        if (!this.inMatrixEnvironment) this.printRightBracket();
    }

    @Override
    public void revisit(ASTMathArithmeticMatrixEEPowExpression node) {
        this.printer.print("^{\\circ~");
    }

    @Override
    public void endVisit(ASTMathArithmeticMatrixEEPowExpression node) {
        this.printer.print("}");
    }

    @Override
    public void revisit(ASTModuloExpression node) {
        this.printer.print("~\\text{mod}~");
    }

    @Override
    public void revisit(ASTMathArithmeticMatrixLeftDivideExpression node) {
        this.printSpace();
        this.printer.print("~\\backslash~");
        this.printSpace();
    }

    @Override
    public void visit(ASTMathForLoopHead node) {
        this.printer.print(node.getName());
        this.printSpace();
        this.printer.print("=");
        this.printSpace();
    }

    @Override
    public void visit(ASTBracketExpression node) {
        boolean nextParenthesis = this.nextParentheses.empty() || this.nextParentheses.peek();

        if (nextParenthesis) {
            this.printer.print("\\left(");
            this.printedParentheses.add(node);
        }
    }

    @Override
    public void endVisit(ASTBracketExpression node) {
        if (this.printedParentheses.contains(node)) {
            this.printer.print("\\right)");
            this.printedParentheses.remove(node);
        }
    }

    @Override
    public void revisit(ASTLessThanExpression node) {
        this.printSpace();
        this.printer.print("<");
        this.printSpace();
    }

    @Override
    public void revisit(ASTLessEqualExpression node) {
        this.printSpace();
        this.printer.print("\\leqslant");
        this.printSpace();
    }

    @Override
    public void revisit(ASTGreaterThanExpression node) {
        this.printSpace();
        this.printer.print(">");
        this.printSpace();
    }

    @Override
    public void revisit(ASTGreaterEqualExpression node) {
        this.printSpace();
        this.printer.print("\\geqslant");
        this.printSpace();
    }

    @Override
    public void revisit(ASTBooleanOrOpExpression node) {
        this.printSpace();
        this.printer.print("\\lor");
        this.printSpace();
    }

    @Override
    public void revisit(ASTBooleanAndOpExpression node) {
        this.printSpace();
        this.printer.print("\\land");
        this.printSpace();
    }

    @Override
    public void revisit(ASTNotEqualsExpression node) {
        this.printSpace();
        this.printer.print("\\neq");
        this.printSpace();
    }

    @Override
    public void visit(ASTMathIfExpression node) { /* NOOP */ }

    @Override
    public void visit(ASTMathElseIfExpression node) { /* NOOP */ }

    @Override
    public void endVisit(ASTMathArithmeticMatrixComplexTransposeExpression node) {
        this.printer.print("^T");
    }

    @Override
    public void revisit(ASTMathArithmeticMatrixEEMultExpression node) {
        this.printSpace();
        this.printer.print("\\circ");
        this.printSpace();
    }

    @Override
    public void revisit(ASTMathArithmeticMatrixEERightDivideExpression node) {
        this.printSpace();
        this.printer.print("\\circ");
        this.printSpace();
    }

    @Override
    public void endVisit(ASTMathArithmeticMatrixEERightDivideExpression node) {
        this.printer.print("^{\\circ(-1)}");
    }

    @Override
    public void visit(ASTNameExpression node) {
        String printedNode = String.format("\\text{%s}", node.getName());

        this.printer.print(printedNode);
    }

    @Override
    public void visit(ASTUnitBaseDimWithPrefix node) {
        this.printer.print("\\;");
        this.printer.print(node.getName());
    }

    @Override
    public void endVisit(ASTDimension node) {
        this.printRightCurlyBracket();
        this.printer.print("\\;");
    }
}

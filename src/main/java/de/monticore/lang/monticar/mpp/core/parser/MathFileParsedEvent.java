/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.mpp.core.parser;

import de.monticore.lang.math._ast.ASTMathCompilationUnit;

public class MathFileParsedEvent {
    public final ASTMathCompilationUnit ast;

    public MathFileParsedEvent(ASTMathCompilationUnit ast) {
        this.ast = ast;
    }
}

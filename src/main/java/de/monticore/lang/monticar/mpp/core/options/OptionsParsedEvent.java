/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.mpp.core.options;

import org.apache.commons.cli.ParseException;

import java.nio.file.Path;

public class OptionsParsedEvent {
    public final Path modelPath;
    public final Path outputPath;

    public OptionsParsedEvent(Path modelPath, Path outputPath) throws ParseException {
        this.modelPath = modelPath;
        this.outputPath = outputPath;
    }
}

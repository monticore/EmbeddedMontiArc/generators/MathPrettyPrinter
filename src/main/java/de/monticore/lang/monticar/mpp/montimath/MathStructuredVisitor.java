/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.mpp.montimath;

import de.monticore.commonexpressions._ast.*;
import de.monticore.expressionsbasis._ast.ASTExpression;
import de.monticore.lang.math._ast.*;
import de.monticore.lang.math._visitor.MathVisitor;
import de.monticore.lang.matrix._ast.ASTMathMatrixAccess;
import de.monticore.lang.matrix._ast.ASTMathMatrixAccessExpression;
import de.monticore.lang.matrix._ast.ASTMathMatrixValueExplicitExpression;
import de.monticore.lang.matrix._ast.ASTMathVectorExpression;
import de.monticore.lang.matrixexpressions._ast.*;
import de.monticore.lang.monticar.ranges._ast.ASTRange;
import de.monticore.lang.monticar.types2._ast.ASTDimension;
import de.monticore.literals.literals._ast.ASTSignedIntLiteral;
import de.monticore.numberunit._ast.*;
import de.monticore.types.types._ast.ASTImportStatement;
import de.monticore.types.types._ast.ASTQualifiedName;

import java.util.Iterator;

public interface MathStructuredVisitor extends MathVisitor {
    @Override
    default MathStructuredVisitor getRealThis() { return this; }

    @Override
    default void handle(ASTMathCompilationUnit node) {
        this.getRealThis().visit(node);
        this.getRealThis().traversePartOne(node);
        this.getRealThis().revisit(node);
        this.getRealThis().traversePartTwo(node);
        this.getRealThis().endVisit(node);
    }

    default void traversePartOne(ASTMathCompilationUnit node) {
        if (node.getPackageOpt().isPresent()) {
            ((ASTQualifiedName)node.getPackageOpt().get()).accept(this.getRealThis());
        }
    }

    default void revisit(ASTMathCompilationUnit node) {}

    default void traversePartTwo(ASTMathCompilationUnit node) {
        Iterator iter_importStatements = node.getImportStatementList().iterator();

        while(iter_importStatements.hasNext()) {
            ((ASTImportStatement)iter_importStatements.next()).accept(this.getRealThis());
        }

        if (null != node.getMathScript()) {
            node.getMathScript().accept(this.getRealThis());
        }
    }

    @Override
    default void handle(ASTMathAssignmentDeclarationStatement node) {
        this.getRealThis().visit(node);
        this.getRealThis().traversePartOne(node);
        this.getRealThis().revisit(node);
        this.getRealThis().traversePartTwo(node);
        this.getRealThis().endVisit(node);
    }

    default void revisit(ASTMathAssignmentDeclarationStatement node) {}

    default void traversePartOne(ASTMathAssignmentDeclarationStatement node) {
        if (null != node.getType()) {
            node.getType().accept(this.getRealThis());
        }
    }

    default void traversePartTwo(ASTMathAssignmentDeclarationStatement node) {
        if (null != node.getMathAssignmentOperator()) {
            node.getMathAssignmentOperator().accept(this.getRealThis());
        }

        if (null != node.getExpression()) {
            node.getExpression().accept(this.getRealThis());
        }
    }

    default void traverse(ASTMathVectorExpression node) {
        if (null != node.getStart()) {
            node.getStart().accept(this.getRealThis());
            this.getRealThis().revisit(node);
        }

        if (node.getStepsOpt().isPresent()) {
            ((ASTExpression)node.getStepsOpt().get()).accept(this.getRealThis());
            this.getRealThis().revisit(node);
        }

        if (null != node.getEnd()) {
            node.getEnd().accept(this.getRealThis());
        }

    }

    default void revisit(ASTMathVectorExpression node) {}

    @Override
    default void handle(ASTLessThanExpression node) {
        this.getRealThis().visit(node);
        this.getRealThis().traversePartOne(node);
        this.getRealThis().revisit(node);
        this.getRealThis().traversePartTwo(node);
        this.getRealThis().endVisit(node);
    }

    default void revisit(ASTLessThanExpression node) {}

    default void traversePartOne(ASTLessThanExpression node) {
        if (null != node.getLeftExpression()) {
            node.getLeftExpression().accept(this.getRealThis());
        }
    }

    default void traversePartTwo(ASTLessThanExpression node) {
        if (null != node.getRightExpression()) {
            node.getRightExpression().accept(this.getRealThis());
        }
    }

    @Override
    default void handle(ASTLessEqualExpression node) {
        this.getRealThis().visit(node);
        this.getRealThis().traversePartOne(node);
        this.getRealThis().revisit(node);
        this.getRealThis().traversePartTwo(node);
        this.getRealThis().endVisit(node);
    }

    default void revisit(ASTLessEqualExpression node) {}

    default void traversePartOne(ASTLessEqualExpression node) {
        if (null != node.getLeftExpression()) {
            node.getLeftExpression().accept(this.getRealThis());
        }
    }

    default void traversePartTwo(ASTLessEqualExpression node) {
        if (null != node.getRightExpression()) {
            node.getRightExpression().accept(this.getRealThis());
        }
    }

    @Override
    default void handle(ASTMultExpression node) {
        this.getRealThis().traversePartOne(node);
        this.getRealThis().visit(node);
        this.getRealThis().traversePartTwo(node);
        this.getRealThis().endVisit(node);
    }

    default void traversePartOne(ASTMultExpression node) {
        if (null != node.getLeftExpression()) {
            node.getLeftExpression().accept(this.getRealThis());
        }
    }

    default void traversePartTwo(ASTMultExpression node) {
        if (null != node.getRightExpression()) {
            node.getRightExpression().accept(this.getRealThis());
        }
    }

    @Override
    default void handle(ASTMinusExpression node) {
        this.getRealThis().traversePartOne(node);
        this.getRealThis().visit(node);
        this.getRealThis().traversePartTwo(node);
        this.getRealThis().endVisit(node);
    }

    default void traversePartOne(ASTMinusExpression node) {
        if (null != node.getLeftExpression()) {
            node.getLeftExpression().accept(this.getRealThis());
        }
    }

    default void traversePartTwo(ASTMinusExpression node) {
        if (null != node.getRightExpression()) {
            node.getRightExpression().accept(this.getRealThis());
        }
    }

    @Override
    default void handle(ASTMathDeclarationStatement node) {
        this.getRealThis().visit(node);
        this.getRealThis().traverse(node);
        this.getRealThis().endVisit(node);
    }

    @Override
    default void handle(ASTPlusExpression node) {
        this.getRealThis().traversePartOne(node);
        this.getRealThis().visit(node);
        this.getRealThis().traversePartTwo(node);
        this.getRealThis().endVisit(node);
    }

    default void traversePartOne(ASTPlusExpression node) {
        if (null != node.getLeftExpression()) {
            node.getLeftExpression().accept(this.getRealThis());
        }
    }

    default void traversePartTwo(ASTPlusExpression node) {
        if (null != node.getRightExpression()) {
            node.getRightExpression().accept(this.getRealThis());
        }
    }

    @Override
    default void traverse(ASTDimension node) {
        Iterator iter_matrixDims = node.getMatrixDimList().iterator();

        while(iter_matrixDims.hasNext()) {
            ((ASTExpression)iter_matrixDims.next()).accept(this.getRealThis());
            if (iter_matrixDims.hasNext()) this.getRealThis().revisit(node);
        }

        if (node.getVecDimOpt().isPresent()) {
            ((ASTExpression)node.getVecDimOpt().get()).accept(this.getRealThis());
            if (iter_matrixDims.hasNext()) this.getRealThis().revisit(node);
        }
    }

    default void revisit(ASTDimension node) {}

    @Override
    default void traverse(ASTMathMatrixValueExplicitExpression node) {
        Iterator iter_mathMatrixAccessExpressions = node.getMathMatrixAccessExpressionList().iterator();

        while(iter_mathMatrixAccessExpressions.hasNext()) {
            ((ASTMathMatrixAccessExpression)iter_mathMatrixAccessExpressions.next()).accept(this.getRealThis());
            if (iter_mathMatrixAccessExpressions.hasNext()) this.getRealThis().revisit(node);
        }

    }

    default void revisit(ASTMathMatrixValueExplicitExpression node) {}

    @Override
    default void traverse(ASTMathMatrixAccessExpression node) {
        Iterator iter_mathMatrixAccesss = node.getMathMatrixAccessList().iterator();

        while(iter_mathMatrixAccesss.hasNext()) {
            ((ASTMathMatrixAccess)iter_mathMatrixAccesss.next()).accept(this.getRealThis());
            if (iter_mathMatrixAccesss.hasNext()) this.getRealThis().revisit(node);
        }

    }

    default void revisit(ASTMathMatrixAccessExpression node) {}

    default void traverse(ASTRange node) {
        if (null != node.getMin()) {
            node.getMin().accept(this.getRealThis());
            this.getRealThis().revisit(node);
        }

        if (node.getStepOpt().isPresent()) {
            ((ASTExpression)node.getStepOpt().get()).accept(this.getRealThis());
            this.getRealThis().revisit(node);
        }

        if (null != node.getMax()) {
            node.getMax().accept(this.getRealThis());
        }

    }

    default void revisit(ASTRange node) {}

    default void traverse(ASTSIUnitBasic node) {
        if (node.getUnitBaseDimWithPrefixOpt().isPresent()) {
            ((ASTUnitBaseDimWithPrefix)node.getUnitBaseDimWithPrefixOpt().get()).accept(this.getRealThis());
            this.getRealThis().revisit(node);
        }

        if (node.getOfficallyAcceptedUnitOpt().isPresent()) {
            ((ASTOfficallyAcceptedUnit)node.getOfficallyAcceptedUnitOpt().get()).accept(this.getRealThis());
            this.getRealThis().revisit(node);
        }

        if (node.getSignedIntLiteralOpt().isPresent()) {
            ((ASTSignedIntLiteral)node.getSignedIntLiteralOpt().get()).accept(this.getRealThis());
        }
    }

    default void revisit(ASTSIUnitBasic node) {}

    @Override
    default void traverse(ASTSIUnit node) {
        Iterator iter_timeDivs = node.getTimeDivList().iterator();
        Iterator iter_sIUnitBasics = node.getSIUnitBasicList().iterator();

        while(iter_sIUnitBasics.hasNext()) {
            ((ASTSIUnitBasic)iter_sIUnitBasics.next()).accept(this.getRealThis());
            if (iter_timeDivs.hasNext()) ((ASTTimeDiv)iter_timeDivs.next()).accept(this.getRealThis());
        }

        if (node.getSiUnitDimensionlessOpt().isPresent()) {
            ((ASTSiUnitDimensionless)node.getSiUnitDimensionlessOpt().get()).accept(this.getRealThis());
        }

    }

    @Override
    default void traverse(ASTEqualsExpression node) {
        if (null != node.getLeftExpression()) {
            node.getLeftExpression().accept(this.getRealThis());
            this.getRealThis().revisit(node);
        }

        if (null != node.getRightExpression()) {
            node.getRightExpression().accept(this.getRealThis());
        }

    }

    default void revisit(ASTEqualsExpression node) {}

    @Override
    default void traverse(ASTGreaterEqualExpression node) {
        if (null != node.getLeftExpression()) {
            node.getLeftExpression().accept(this.getRealThis());
            this.getRealThis().revisit(node);
        }

        if (null != node.getRightExpression()) {
            node.getRightExpression().accept(this.getRealThis());
        }

    }

    default void revisit(ASTGreaterEqualExpression node) {}

    @Override
    default void traverse(ASTGreaterThanExpression node) {
        if (null != node.getLeftExpression()) {
            node.getLeftExpression().accept(this.getRealThis());
            this.getRealThis().revisit(node);
        }

        if (null != node.getRightExpression()) {
            node.getRightExpression().accept(this.getRealThis());
        }

    }

    default void revisit(ASTGreaterThanExpression node) {}

    @Override
    default void traverse(ASTBooleanOrOpExpression node) {
        if (null != node.getLeftExpression()) {
            node.getLeftExpression().accept(this.getRealThis());
            this.getRealThis().revisit(node);
        }

        if (null != node.getRightExpression()) {
            node.getRightExpression().accept(this.getRealThis());
        }

    }

    default void revisit(ASTBooleanOrOpExpression node) {}

    @Override
    default void traverse(ASTBooleanAndOpExpression node) {
        if (null != node.getLeftExpression()) {
            node.getLeftExpression().accept(this.getRealThis());
            this.getRealThis().revisit(node);
        }

        if (null != node.getRightExpression()) {
            node.getRightExpression().accept(this.getRealThis());
        }

    }

    default void revisit(ASTBooleanAndOpExpression node) {}

    @Override
    default void traverse(ASTNotEqualsExpression node) {
        if (null != node.getLeftExpression()) {
            node.getLeftExpression().accept(this.getRealThis());
            this.getRealThis().revisit(node);
        }

        if (null != node.getRightExpression()) {
            node.getRightExpression().accept(this.getRealThis());
        }

    }

    default void revisit(ASTNotEqualsExpression node) {}

    @Override
    default void traverse(ASTMathArithmeticPowerOfExpression node) {
        if (null != node.getLeftExpression()) {
            node.getLeftExpression().accept(this.getRealThis());
            this.getRealThis().revisit(node);
        }

        if (null != node.getRightExpression()) {
            node.getRightExpression().accept(this.getRealThis());
        }
    }

    default void revisit(ASTMathArithmeticPowerOfExpression node) {}

    @Override
    default void traverse(ASTModuloExpression node) {
        if (null != node.getLeftExpression()) {
            node.getLeftExpression().accept(this.getRealThis());
            this.getRealThis().revisit(node);
        }

        if (null != node.getRightExpression()) {
            node.getRightExpression().accept(this.getRealThis());
        }

    }

    default void revisit(ASTModuloExpression node) {}

    @Override
    default void traverse(ASTDivideExpression node) {
        if (null != node.getLeftExpression()) {
            node.getLeftExpression().accept(this.getRealThis());
            this.getRealThis().revisit(node);
        }

        if (null != node.getRightExpression()) {
            node.getRightExpression().accept(this.getRealThis());
        }
    }

    default void revisit(ASTDivideExpression node) {}

    @Override
    default void traverse(ASTMathArithmeticMatrixEEPowExpression node) {
        Iterator iter_expressions = node.getExpressionList().iterator();

        while(iter_expressions.hasNext()) {
            ((ASTExpression)iter_expressions.next()).accept(this.getRealThis());
            if (iter_expressions.hasNext()) this.getRealThis().revisit(node);
        }

    }

    default void revisit(ASTMathArithmeticMatrixEEPowExpression node) {}

    @Override
    default void traverse(ASTMathArithmeticMatrixLeftDivideExpression node) {
        Iterator iter_expressions = node.getExpressionList().iterator();

        while(iter_expressions.hasNext()) {
            ((ASTExpression)iter_expressions.next()).accept(this.getRealThis());
            if (iter_expressions.hasNext()) this.getRealThis().revisit(node);
        }

    }

    default void revisit(ASTMathArithmeticMatrixLeftDivideExpression node) {}

    @Override
    default void traverse(ASTMathArithmeticMatrixEEMultExpression node) {
        Iterator iter_expressions = node.getExpressionList().iterator();

        while(iter_expressions.hasNext()) {
            ((ASTExpression)iter_expressions.next()).accept(this.getRealThis());
            if (iter_expressions.hasNext()) this.getRealThis().revisit(node);
        }

    }

    default void revisit(ASTMathArithmeticMatrixEEMultExpression node) {}

    @Override
    default void traverse(ASTMathArithmeticMatrixEERightDivideExpression node) {
        Iterator iter_expressions = node.getExpressionList().iterator();

        while(iter_expressions.hasNext()) {
            ((ASTExpression)iter_expressions.next()).accept(this.getRealThis());
            if (iter_expressions.hasNext()) this.getRealThis().revisit(node);
        }

    }

    default void revisit(ASTMathArithmeticMatrixEERightDivideExpression node) {}

    @Override
    default void traverse(ASTMathArithmeticMatrixEELeftDivideExpression node) {
        Iterator iter_expressions = node.getExpressionList().iterator();

        while(iter_expressions.hasNext()) {
            ((ASTExpression)iter_expressions.next()).accept(this.getRealThis());
            if (iter_expressions.hasNext()) this.getRealThis().revisit(node);
        }

    }

    default void revisit(ASTMathArithmeticMatrixEELeftDivideExpression node) {}

    @Override
    default void handle(ASTMathIfExpression node) {
        this.getRealThis().visit(node);
        this.getRealThis().traversePartOne(node);
        this.getRealThis().revisit(node);
        this.getRealThis().traversePartTwo(node);
        this.getRealThis().endVisit(node);
    }

    default void traversePartOne(ASTMathIfExpression node) {
        if (null != node.getCondition()) {
            node.getCondition().accept(this.getRealThis());
        }
    }

    default void traversePartTwo(ASTMathIfExpression node) {
        Iterator iter_bodys = node.getBodyList().iterator();

        while(iter_bodys.hasNext()) {
            ((ASTStatement)iter_bodys.next()).accept(this.getRealThis());
        }
    }

    default void revisit(ASTMathIfExpression node)
    {

    }

    @Override
    default void handle(ASTMathElseIfExpression node) {
        this.getRealThis().visit(node);
        this.getRealThis().traversePartOne(node);
        this.getRealThis().revisit(node);
        this.getRealThis().traversePartTwo(node);
        this.getRealThis().endVisit(node);
    }

    default void traversePartOne(ASTMathElseIfExpression node) {
        if (null != node.getCondition()) {
            node.getCondition().accept(this.getRealThis());
        }
    }

    default void traversePartTwo(ASTMathElseIfExpression node) {
        Iterator iter_bodys = node.getBodyList().iterator();

        while(iter_bodys.hasNext()) {
            ((ASTStatement)iter_bodys.next()).accept(this.getRealThis());
        }
    }

    default void revisit(ASTMathElseIfExpression node)
    {

    }

    /*@Override
    default void handle(ASTMathElseExpression node) {
        this.getRealThis().visit(node);
        this.getRealThis().traversePartOne(node);
        this.getRealThis().revisit(node);
        this.getRealThis().traversePartTwo(node);
        this.getRealThis().endVisit(node);
    }

    default void traverse(ASTMathElseExpression node) {
        Iterator iter_bodys = node.getBodyList().iterator();

        while(iter_bodys.hasNext()) {
            ((ASTStatement)iter_bodys.next()).accept(this.getRealThis());
        }

    }*/
}

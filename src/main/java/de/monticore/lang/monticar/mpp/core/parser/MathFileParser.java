/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.mpp.core.parser;

import com.google.common.eventbus.Subscribe;
import de.monticore.lang.math._ast.ASTMathCompilationUnit;
import de.monticore.lang.math._parser.MathParser;
import de.monticore.lang.monticar.mpp.core.events.EventsService;
import de.monticore.lang.monticar.mpp.core.visitor.MathFileVisitedEvent;

import java.io.IOException;
import java.util.Optional;

public class MathFileParser {
    protected final EventsService eventsService;
    protected final MathParser parser;

    public MathFileParser(EventsService eventsService) {
        this.eventsService = eventsService;
        this.parser = new MathParser();

        this.eventsService.register(this);
    }


    @Subscribe
    public void onMathFileVisited(MathFileVisitedEvent event) throws IOException {
        String filename = event.file.toString();
        Optional<ASTMathCompilationUnit> astOptional = this.parser.parse(filename);

        astOptional.ifPresent(ast -> this.eventsService.post(new MathFileParsedEvent(ast)));
    }
}

/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.mpp.core.visitor;

import java.nio.file.Path;

public class MathFileVisitedEvent {
    public final Path file;

    public MathFileVisitedEvent(Path file) {
        this.file = file;
    }
}

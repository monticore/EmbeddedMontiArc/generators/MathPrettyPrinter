/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.mpp.core.application;

public class ApplicationConfigureEvent {
    public final String[] arguments;

    public ApplicationConfigureEvent(String[] arguments) {
        this.arguments = arguments;
    }
}

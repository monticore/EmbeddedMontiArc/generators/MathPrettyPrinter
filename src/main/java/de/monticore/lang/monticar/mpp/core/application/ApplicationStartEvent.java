/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.mpp.core.application;

public class ApplicationStartEvent {
    public final String[] arguments;

    public ApplicationStartEvent(String[] arguments) {
        this.arguments = arguments;
    }
}

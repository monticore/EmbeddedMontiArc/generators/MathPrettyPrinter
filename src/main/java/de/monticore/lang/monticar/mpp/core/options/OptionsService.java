/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.mpp.core.options;

import com.google.common.eventbus.Subscribe;
import de.monticore.lang.math._ast.ASTMathCompilationUnit;
import de.monticore.lang.monticar.mpp.core.application.ApplicationConfigureEvent;
import de.monticore.lang.monticar.mpp.core.events.EventsService;
import de.monticore.lang.monticar.mpp.core.options.OptionsParsedEvent;
import org.apache.commons.cli.*;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class OptionsService {
    protected final EventsService eventsService;
    protected final Options options;

    protected Path modelPath;
    protected Path outputPath;
    protected boolean internal;

    public OptionsService(EventsService eventsService) {
        this.eventsService = eventsService;
        this.options = new Options();

        this.eventsService.register(this);
        this.addOptions();
    }

    protected void addOptions() {
        this.options.addOption("mp", "model-path", true,
                "Absolute Path to the package root of the models.");
        this.options.addOption("out", "output-path", true,
                "Absolute Path to the root directory of the output.");
        this.options.addOption("i", "internal", false,
                "If set, runs the application in internal mode.");
    }

    public Path getModelPath() {
        return this.modelPath;
    }

    public Path getOutputPath() {
        return this.outputPath;
    }

    public Path getOutputPath(ASTMathCompilationUnit ast) {
        String packagePath = ast.getPackage().toString().replace(".", File.separator);
        String artifactName = ast.getMathScript().getName();
        Path relativePath = Paths.get(packagePath, artifactName);

        return this.outputPath.resolve(relativePath);
    }

    public boolean isInternal() {
        return this.internal;
    }

    @Subscribe
    public void onApplicationConfigure(ApplicationConfigureEvent event) throws ParseException {
        CommandLineParser parser = new DefaultParser();
        CommandLine commandLine = parser.parse(this.options, event.arguments);

        String modelPath = (String)commandLine.getParsedOptionValue("mp");
        String outputPath = (String)commandLine.getParsedOptionValue("out");

        this.modelPath = Paths.get(modelPath);
        this.outputPath = Paths.get(outputPath);
        this.internal = commandLine.hasOption("i");

        OptionsParsedEvent optionsEvent = new OptionsParsedEvent(this.modelPath, this.outputPath);

        this.eventsService.post(optionsEvent);
    }
}

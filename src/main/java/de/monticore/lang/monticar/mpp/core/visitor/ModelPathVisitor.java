/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.mpp.core.visitor;

import com.google.common.eventbus.Subscribe;
import de.monticore.lang.monticar.mpp.core.application.ApplicationStartEvent;
import de.monticore.lang.monticar.mpp.core.options.OptionsParsedEvent;
import de.monticore.lang.monticar.mpp.core.events.EventsService;
import de.monticore.lang.monticar.mpp.core.options.OptionsService;
import org.apache.commons.cli.ParseException;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ModelPathVisitor extends SimpleFileVisitor<Path> {
    protected final EventsService eventsService;
    protected final OptionsService optionsService;

    public ModelPathVisitor(EventsService eventsService, OptionsService optionsService) {
        this.eventsService = eventsService;
        this.optionsService = optionsService;

        this.eventsService.register(this);
    }

    protected void visit() throws IOException {
        Path modelPath = this.optionsService.getModelPath();

        Files.walkFileTree(modelPath, this);
    }


    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
        String filePath = file.toString();

        if (filePath.endsWith(".m")) {
            MathFileVisitedEvent event = new MathFileVisitedEvent(file);

            this.eventsService.post(event);
        }

        return FileVisitResult.CONTINUE;
    }


    @Subscribe
    public void onApplicationStart(ApplicationStartEvent event) throws IOException {
        this.visit();
    }
}

/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.mpp.core.tex;

import de.monticore.lang.math._ast.ASTMathCompilationUnit;
import de.monticore.lang.monticar.mpp.core.options.OptionsService;
import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGeneratorContext;
import org.apache.batik.svggen.SVGGraphics2D;
import org.scilab.forge.jlatexmath.DefaultTeXFont;
import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;
import org.scilab.forge.jlatexmath.cyrillic.CyrillicRegistration;
import org.scilab.forge.jlatexmath.greek.GreekRegistration;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;

import java.awt.*;
import java.io.*;
import java.nio.file.Path;

public class SVGGenerator {
    protected final OptionsService optionsService;

    protected Path outputPath;

    public SVGGenerator(OptionsService optionsService) {
        this.optionsService = optionsService;
    }

    public void setContext(ASTMathCompilationUnit ast) {
        this.outputPath = this.optionsService.getOutputPath(ast);

        File outputDirectory = this.outputPath.toFile();

        if (!outputDirectory.exists()) outputDirectory.mkdirs();
    }

    public String generate(String latex) throws IOException {
        DOMImplementation dom = GenericDOMImplementation.getDOMImplementation();
        Document document = dom.createDocument("http://www.w3.org/2000/svg", "svg", null);
        SVGGeneratorContext context = SVGGeneratorContext.createDefault(document);
        SVGGraphics2D graphics = new SVGGraphics2D(context, true);

        DefaultTeXFont.registerAlphabet(new CyrillicRegistration());
        DefaultTeXFont.registerAlphabet(new GreekRegistration());

        TeXFormula formula = new TeXFormula(latex);
        TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20, 0, new Color(206,206,206));

        icon.setInsets(new Insets(0,0,0,0));

        graphics.setSVGCanvasSize(new Dimension(icon.getIconWidth(), icon.getIconHeight()));
        graphics.setColor(new Color(37,37,37, 0));
        graphics.fillRect(0, 0, icon.getIconWidth(), icon.getIconHeight());

        icon.paintIcon(null, graphics, 0, 0);

        String filename = String.format("%d.svg", latex.hashCode());
        Path outputPath = this.outputPath.resolve(filename);
        FileOutputStream stream = new FileOutputStream(outputPath.toString());
        Writer out = new OutputStreamWriter(stream, "UTF-8");

        graphics.stream(out, true);
        stream.flush();
        stream.close();

        return filename;
    }
}

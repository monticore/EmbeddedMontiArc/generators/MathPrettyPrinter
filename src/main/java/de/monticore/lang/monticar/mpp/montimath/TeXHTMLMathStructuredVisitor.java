/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.mpp.montimath;

import de.monticore.lang.math._ast.*;


public interface TeXHTMLMathStructuredVisitor extends MathStructuredVisitor {
    @Override
    default void traversePartOne(ASTMathAssignmentDeclarationStatement node) {}

    @Override
    default void traversePartTwo(ASTMathAssignmentDeclarationStatement node) {}

    @Override
    default void traverse(ASTMathAssignmentStatement node) {}

    @Override
    default void traverse(ASTMathDeclarationStatement node) {}

    @Override
    default void traverse(ASTMathForLoopHead node) {}

    /*@Override
    default void traverse(ASTMathIfExpression node) {
        for(ASTStatement statement: node.getBodyList())
        {
            if (null != node.getBody(0)) {
                node.getBody(0).accept(this.getRealThis());
            }
        }

    }

    @Override
    default void traverse(ASTMathElseIfExpression node) {
        if (null != node.getBody(0)) {
            node.getBody(0).accept(this.getRealThis());
        }
    }*/

    @Override
    default void handle(ASTMathIfExpression node) {
        this.getRealThis().visit(node);
        this.getRealThis().revisit(node);
        this.getRealThis().traversePartTwo(node);
        this.getRealThis().endVisit(node);
    }

    @Override
    default void handle(ASTMathElseIfExpression node) {
        this.getRealThis().visit(node);
        this.getRealThis().revisit(node);
        this.getRealThis().traversePartTwo(node);
        this.getRealThis().endVisit(node);
    }
}

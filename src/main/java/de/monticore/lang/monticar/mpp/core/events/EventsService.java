/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.mpp.core.events;

import com.google.common.eventbus.EventBus;

public class EventsService {
    protected final EventBus eventBus;

    public EventsService() {
        this.eventBus = new EventBus(new EventsExceptionHandler());
    }

    public void post(Object event) {
        this.eventBus.post(event);
    }

    public void register(Object object) {
        this.eventBus.register(object);
    }

    public void unregister(Object object) {
        this.eventBus.unregister(object);
    }
}

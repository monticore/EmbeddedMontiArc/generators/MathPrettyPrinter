/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.mpp.montimath;

public class HTMLMathPrettyPrinter extends MathPrettyPrinter {
    protected String getWrappedKeyword(String keyword) {
        return String.format("<span class=\"keyword\">%s</span>", keyword);
    }

    protected String getWrappedComment(String comment) {
        return String.format("<span class=\"comment\">%s</span>", comment);
    }


    @Override
    protected void printPackage() {
        this.printer.print(this.getWrappedKeyword("package"));
    }

    @Override
    protected void printScript() {
        this.printer.print(this.getWrappedKeyword("script"));
    }

    @Override
    protected void printImport() {
        this.printer.print(this.getWrappedKeyword("import"));
    }

    @Override
    protected void printEnd() {
        this.printer.print(this.getWrappedKeyword("end"));
    }

    @Override
    protected void printFor() {
        this.printer.print(this.getWrappedKeyword("for"));
    }

    @Override
    protected void printIf() {
        this.printer.print(this.getWrappedKeyword("if"));
    }

    @Override
    protected void printElse() {
        this.printer.print(this.getWrappedKeyword("else"));
    }

    @Override
    protected void printElseIf() {
        this.printer.print(this.getWrappedKeyword("elseif"));
    }

    @Override
    protected void printComment(String comment) {
        String[] lines = comment.split("\n");

        if (lines.length > 0) lines[0] = lines[0].trim();

        for (String line : lines) {
            this.printer.println(this.getWrappedComment(line));
        }
    }
}

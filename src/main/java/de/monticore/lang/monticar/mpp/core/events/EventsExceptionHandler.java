/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.mpp.core.events;

import com.google.common.eventbus.SubscriberExceptionContext;
import com.google.common.eventbus.SubscriberExceptionHandler;
import de.se_rwth.commons.logging.Log;

public class EventsExceptionHandler implements SubscriberExceptionHandler {
    @Override
    public void handleException(Throwable exception, SubscriberExceptionContext context) {
        Log.error(exception.getMessage());
    }
}

/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.mpp.core.application;

import de.monticore.lang.monticar.mpp.core.events.EventsService;
import de.monticore.lang.monticar.mpp.core.generator.Generator;
import de.monticore.lang.monticar.mpp.core.options.OptionsService;
import de.monticore.lang.monticar.mpp.core.visitor.ModelPathVisitor;

public class Application {
    protected final EventsService eventsService;
    protected final OptionsService optionsService;
    protected final Generator generator;
    protected final ModelPathVisitor modelPathVisitor;

    public Application() {
        this.eventsService = new EventsService();
        this.optionsService = new OptionsService(this.eventsService);
        this.generator = new Generator(this.eventsService, this.optionsService);
        this.modelPathVisitor = new ModelPathVisitor(this.eventsService, this.optionsService);
    }

    public void start(String[] args) {
        ApplicationConfigureEvent configureEvent = new ApplicationConfigureEvent(args);
        ApplicationStartEvent startEvent = new ApplicationStartEvent(args);

        this.eventsService.post(configureEvent);
        this.eventsService.post(startEvent);
    }

    public static void main(String[] args) {
        Application application = new Application();

        application.start(args);
    }
}

/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.mpp.montimath;

import de.monticore.lang.math._ast.ASTMathElseIfExpression;
import de.monticore.lang.math._ast.ASTMathIfExpression;

public interface TeXMathStructuredVisitor extends MathStructuredVisitor {
    /*@Override
    default void traverse(ASTMathIfExpression node) {
        if (null != node.getCondition()) {
            node.getCondition().accept(this.getRealThis());
        }
    }

    @Override
    default void traverse(ASTMathElseIfExpression node) {
        if (null != node.getCondition()) {
            node.getCondition().accept(this.getRealThis());
        }
    }*/
    @Override
    default void handle(ASTMathIfExpression node) {
        this.getRealThis().visit(node);
        this.getRealThis().traversePartOne(node);
        this.getRealThis().revisit(node);
        this.getRealThis().endVisit(node);
    }

    @Override
    default void handle(ASTMathElseIfExpression node) {
        this.getRealThis().visit(node);
        this.getRealThis().traversePartOne(node);
        this.getRealThis().revisit(node);
        this.getRealThis().endVisit(node);
    }
}

/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.mpp.core.generator;

import com.google.common.eventbus.Subscribe;
import de.monticore.generating.GeneratorEngine;
import de.monticore.generating.GeneratorSetup;
import de.monticore.generating.templateengine.GlobalExtensionManagement;
import de.monticore.lang.math._ast.ASTMathCompilationUnit;
import de.monticore.lang.monticar.mpp.core.events.EventsService;
import de.monticore.lang.monticar.mpp.core.options.OptionsParsedEvent;
import de.monticore.lang.monticar.mpp.core.options.OptionsService;
import de.monticore.lang.monticar.mpp.core.parser.MathFileParsedEvent;
import de.monticore.lang.monticar.mpp.core.parser.MathFileParser;
import de.monticore.lang.monticar.mpp.core.visitor.MathFileVisitedEvent;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Generator {
    protected final GeneratorHelper helper;
    protected final OptionsService optionsService;
    protected final MathFileParser parser;

    protected GeneratorEngine generator;

    private int index;
    private int count;

    public Generator(EventsService eventsService, OptionsService optionsService) {
        this.helper = new GeneratorHelper(eventsService, optionsService);
        this.parser = new MathFileParser(eventsService);
        this.optionsService = optionsService;

        this.index = 0;
        this.count = 0;

        eventsService.register(this);
    }

    protected void prepare() {
        GeneratorSetup setup = new GeneratorSetup();
        File outputDirectory = this.optionsService.getOutputPath().toFile();
        GlobalExtensionManagement glex = new GlobalExtensionManagement();

        glex.setGlobalValue("helper", this.helper);

        setup.setGlex(glex);
        setup.setTracing(false);
        setup.setOutputDirectory(outputDirectory);

        this.generator = new GeneratorEngine(setup);
    }

    public void generateIndex() {
        Path outputPath = Paths.get("index.html");

        this.generator.generateNoA("templates/index.ftl", outputPath);
    }

    public void generateScript(ASTMathCompilationUnit node) {
        String packageName = node.getPackage().toString();
        String outputDirectory = packageName.replace(".", File.separator);
        Path outputPath = Paths.get(outputDirectory, node.getMathScript().getName(), "index.html");

        this.generator.generate("templates/script.ftl", outputPath, node);
    }


    @Subscribe
    public void onOptionsParsed(OptionsParsedEvent event) {
        this.prepare();
    }

    @Subscribe
    public void onMathFileVisited(MathFileVisitedEvent event) {
        this.count++;
    }

    @Subscribe
    public void onMathFileParsed(MathFileParsedEvent event) {
        this.generateScript(event.ast);
        this.index++;

        if (this.index == this.count) this.generateIndex();
    }
}

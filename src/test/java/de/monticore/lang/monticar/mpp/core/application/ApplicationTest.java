/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.mpp.core.application;

import de.monticore.lang.monticar.mpp.core.application.Application;
import org.junit.Ignore;
import org.junit.Test;

import java.nio.file.Paths;

public class ApplicationTest {
    @Test
    public void testStart() {
        String modelPath = Paths.get("src/test/resources/models/montimath/vanilla").toAbsolutePath().toString();
        String outputPath = Paths.get("target/generated-sources/application").toAbsolutePath().toString();
        String[] arguments = { "-mp", modelPath, "-out", outputPath };
        Application application = new Application();

        application.start(arguments);
    }
}

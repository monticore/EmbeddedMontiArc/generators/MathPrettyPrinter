/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.mpp.montimath;

import de.monticore.lang.math._ast.ASTMathCompilationUnit;
import de.monticore.lang.math._parser.MathParser;
import de.se_rwth.commons.logging.Log;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

public class HTMLMathPrettyPrinterTest {
    protected static File[] sourceModels;
    protected static File[] targetModels;

    @BeforeClass
    public static void prepare() {
        Path sourcePath = Paths.get("src/test/resources/models/montimath/vanilla").toAbsolutePath();
        Path targetPath = Paths.get("src/test/resources/models/montimath/html").toAbsolutePath();
        File sourceFile = sourcePath.toFile();
        File targetFile = targetPath.toFile();

        sourceModels = sourceFile.listFiles();
        targetModels = targetFile.listFiles();
    }

    @Test
    public void testPrinter() throws IOException {
        int length = sourceModels.length;
        MathParser parser = new MathParser();
        HTMLMathPrettyPrinter printer = new HTMLMathPrettyPrinter();

        for (int i = 0; i < length; i++) {
            File sourceModel = sourceModels[i];
            File targetModel = targetModels[i];
            String modelName = sourceModel.getName();
            String message = String.format("Checking Equality of %s.", modelName);

            if (modelName.equals("Comments.m")) continue;

            Log.info(message, "HTMLMathPrettyPrinterTest");

            List<String> sourceLines = FileUtils.readLines(sourceModel, "UTF-8");
            String source = String.join("\n", sourceLines.subList(1, sourceLines.size()));
            Optional<ASTMathCompilationUnit> astOptional = parser.parse_String(source);
            List<String> targetLines = FileUtils.readLines(targetModel, "UTF-8");
            String input = String.join("\n", targetLines.subList(1, targetLines.size()));
            String output = printer.prettyPrint(astOptional.get());

            assertEquals(input.replaceAll("\\R+", "\n"), output.replaceAll("\\R+", "\n"));
        }
    }
}

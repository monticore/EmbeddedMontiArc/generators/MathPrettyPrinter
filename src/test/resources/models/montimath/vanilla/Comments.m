/* (c) https://github.com/MontiCore/monticore */
package montimath.vanilla;

/*
 * A Dummy Script to show that comments can be shown.
 */
script Comments
    Q(0:10)^{1,5} c = 1:2:10;
    Q x = 0;
    Q y = 0;

    // Iterate over all i in c
    for i = c
        // Iterate over all j in c
        for j = c
            // y might become very small when the
            // entries in c are very large.
            y += 1 / (c(x) * j^i);
        end
        x += 1;
    end
end

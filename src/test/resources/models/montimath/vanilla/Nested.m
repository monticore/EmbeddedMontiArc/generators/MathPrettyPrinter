// (c) https://github.com/MontiCore/monticore 
package montimath.vanilla;

script Nested
    Q sum = 1;

    for i = 1:2:9
        if i < 2
            sum -= sum * i;
        elseif i < 4
            sum += sum * i;
        else
            for j = 1:2:6
                sum += sum * (i - j);
            end
        end
    end
end

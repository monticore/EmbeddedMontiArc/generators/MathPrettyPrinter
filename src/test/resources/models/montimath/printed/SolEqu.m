// (c) https://github.com/MontiCore/monticore 
package montimath.vanilla;

script SolEqu
    Q^{3, 1} A = [3, 6, 2; 1, 2, 8; 7, 9, 4] \ [2; 3; 4];
    Q^{3, 1} Bmat = ([3 / 2, 6 / 2, 2 / 2; 1 / 2, 2 / 2, 8 / 2; 7 / 2, 9 / 2, 4 / 2] + [3 / 2, 6 / 2, 2 / 2; 1 / 2, 2 / 2, 8 / 2; 7 / 2, 9 / 2, 4 / 2]) \ [2; 3; 4];
end

// (c) https://github.com/MontiCore/monticore 
package montimath.vanilla;

script PowerWise
    Q^{2} A = [1, 2].^2;
    Q^{2} Bmat = ([1, 2] * [1, 2; 3, 4]).^2;
end

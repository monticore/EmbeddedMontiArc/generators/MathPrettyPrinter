// (c) https://github.com/MontiCore/monticore 
package montimath.vanilla;

script Hadamard
    Q^{2, 2} A = [1 + 1, 1 * 2; 1 - matC, 1 / matB];
    Q^{2, 2} B = [1 + 1, 1 * 2; 1 - matC, 1 / matB];
    A = A .* B;
    A = A ./ B;
end

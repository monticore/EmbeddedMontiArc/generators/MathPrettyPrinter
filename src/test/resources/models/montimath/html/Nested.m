// (c) https://github.com/MontiCore/monticore 
<span class="keyword">package</span> montimath.vanilla;

<span class="keyword">script</span> Nested
    Q sum = 1;
    <span class="keyword">for</span> i = 1:2:9
        <span class="keyword">if</span> i < 2
            sum -= sum * i;
        <span class="keyword">elseif</span> i < 4
            sum += sum * i;
        <span class="keyword">else</span>
            <span class="keyword">for</span> j = 1:2:6
                sum += sum * (i - j);
            <span class="keyword">end</span>
        <span class="keyword">end</span>
    <span class="keyword">end</span>
<span class="keyword">end</span>

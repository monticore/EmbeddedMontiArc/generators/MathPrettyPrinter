// (c) https://github.com/MontiCore/monticore 
<span class="keyword">package</span> montimath.vanilla;

<span class="keyword">script</span> TimeWise
    Q^{2} A = [2, 2] .* [1, 2];
    Q^{2} Bmat = ([1, 2] + [3, 4]) .* [5, 6];
<span class="keyword">end</span>

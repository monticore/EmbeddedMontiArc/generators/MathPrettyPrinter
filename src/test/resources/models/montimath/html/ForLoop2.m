// (c) https://github.com/MontiCore/monticore 
<span class="keyword">package</span> montimath.vanilla;

<span class="keyword">script</span> ForLoop2
    Q(0 m : 1000 m)^{5} c = [1 m, 3 m, 5 m, 7 m, 9 m];
    Q x = 0;
    Q(0 m^2 : 1000 m^2) y = 0 m * m;
    Q(0 m : 1000 m) z = 0 m;
    <span class="comment">//TODO fix this to work with variable vectors, and not only vectors that are equivalent to range vectors</span>
    <span class="keyword">for</span> i = c
        <span class="keyword">for</span> j = c
            y += j * i;
            z += c(x + 0);
        <span class="keyword">end</span>
        x += 1;
    <span class="keyword">end</span>
<span class="keyword">end</span>

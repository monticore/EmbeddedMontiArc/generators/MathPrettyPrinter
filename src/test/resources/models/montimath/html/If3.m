// (c) https://github.com/MontiCore/monticore 
<span class="keyword">package</span> montimath.vanilla;

<span class="keyword">script</span> If3
    Q cond1 = 1;
    Q cond2 = -1;
    Q result = 0;
    B bool = cond1 == cond2;
    <span class="keyword">if</span> bool
        result = 1;
    <span class="keyword">elseif</span> bool || cond2 < 0
        result = -1;
    <span class="keyword">else</span>
        result = 0;
    <span class="keyword">end</span>
<span class="keyword">end</span>

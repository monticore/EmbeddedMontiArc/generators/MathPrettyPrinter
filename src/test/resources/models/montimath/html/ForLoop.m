// (c) https://github.com/MontiCore/monticore 
<span class="keyword">package</span> montimath.vanilla;

<span class="keyword">script</span> ForLoop
    Q sum = 1;
    <span class="keyword">for</span> i = 1:2:9
        sum += sum * i;
    <span class="keyword">end</span>
<span class="keyword">end</span>

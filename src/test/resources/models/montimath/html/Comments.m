<span class="comment">/*</span>
<span class="comment"> * (c) https://github.com/MontiCore/monticore </span>
<span class="comment"> */</span>
<span class="keyword">package</span> montimath.vanilla;

<span class="comment">/*</span>
<span class="comment"> * A Dummy Script to show that comments can be shown.</span>
<span class="comment"> */</span>
<span class="keyword">script</span> Comments
    Q(0 : 10)^{1, 5} c = 1:2:10;
    Q x = 0;
    Q y = 0;
    <span class="comment">// Iterate over all i in c</span>
    <span class="keyword">for</span> i = c
        <span class="comment">// Iterate over all j in c</span>
        <span class="keyword">for</span> j = c
            <span class="comment">// y might become very small when the</span>
            <span class="comment">// entries in c are very large.</span>
            y += 1 / (c(x) * j^i);
        <span class="keyword">end</span>
        x += 1;
    <span class="keyword">end</span>
<span class="keyword">end</span>

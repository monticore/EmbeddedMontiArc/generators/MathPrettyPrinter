// (c) https://github.com/MontiCore/monticore 
<span class="keyword">package</span> montimath.vanilla;

<span class="keyword">script</span> Hadamard
    Q^{2, 2} A = [1 + 1, 1 * 2; 1 - matC, 1 / matB];
    Q^{2, 2} B = [1 + 1, 1 * 2; 1 - matC, 1 / matB];
    A = A .* B;
    A = A ./ B;
<span class="keyword">end</span>

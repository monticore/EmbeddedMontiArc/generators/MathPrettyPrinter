// (c) https://github.com/MontiCore/monticore 
<span class="keyword">package</span> montimath.vanilla;

<span class="keyword">script</span> ForLoop3
    Q^{3, 3} A = [1, 2, 3; 4, 5, 6; 7, 8, 9];
    Q Cmat = 0;
    <span class="keyword">for</span> i = 0:2
        <span class="keyword">for</span> j = 0:2
            Cmat += A(i, j);
        <span class="keyword">end</span>
    <span class="keyword">end</span>
<span class="keyword">end</span>

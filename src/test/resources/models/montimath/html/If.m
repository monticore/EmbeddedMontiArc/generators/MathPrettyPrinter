// (c) https://github.com/MontiCore/monticore 
<span class="keyword">package</span> montimath.vanilla;

<span class="keyword">script</span> If
    Q cond = 2;
    Q result = 0;
    <span class="keyword">if</span> cond == 1
        result = 1;
    <span class="keyword">elseif</span> cond == 0
        result = -1;
    <span class="keyword">else</span>
        result = 0;
    <span class="keyword">end</span>
<span class="keyword">end</span>

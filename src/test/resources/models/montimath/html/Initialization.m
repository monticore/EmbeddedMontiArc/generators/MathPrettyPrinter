// (c) https://github.com/MontiCore/monticore 
<span class="keyword">package</span> montimath.vanilla;

<span class="keyword">script</span> Initialization
    Q A = 1;
    Q Bmat = 2;
    Q^{2, 2} Cmat = [1, 1; 1, 1];
    Q^{2, 2} D = [1, 2; 3, 4];
    Q rational;
<span class="keyword">end</span>

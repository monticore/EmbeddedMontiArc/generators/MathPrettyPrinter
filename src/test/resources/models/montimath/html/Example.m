// (c) https://github.com/MontiCore/monticore 
<span class="keyword">package</span> montimath.vanilla;

<span class="keyword">script</span> Example
    Q(0 : 10)^{1, 5} c = 1:2:10;
    Q x = 0;
    Q y = 0;
    <span class="keyword">for</span> i = c
        <span class="keyword">for</span> j = c
            y += 1 / (c(x) * j^i);
        <span class="keyword">end</span>
        x += 1;
    <span class="keyword">end</span>
<span class="keyword">end</span>

<!-- (c) https://github.com/MontiCore/monticore -->
## https://embeddedmontiarc.github.io/webspace/MathPrettyPrinter/

bitte generierte HTML-Dateien in gh-pages branch pushen, dann wird es gepublished

# PrettyPrinter for MontiMath including Decorators

should be similar to: http://www.se-rwth.de/materials/ema_compiler/Matlab/Ng_Jordan_Weiss.html

For this code the following two static HTML views (inclusive buttons to switch between the two views) should be generated:

```
script example2

    Q(0:10)^{5,1} c = 1:2:10;
    Q x = 0;
    Q y = 0;

    for i = c
      for j = c
        y+=1/(c(x)*j^i);
      end
      x+=1;
    end
end
```

# View 1 (uninterpreted code with syntax highlighting and line numbers)
![BildSkript](/uploads/de32b51577a53b383ac8971d82304549/BildSkript.PNG)

# View 2 (as interpreted math code with syntax highlighting and line numbers)
![image](/uploads/2224a62c89a5d4feb592b75950199e96/image.png)

* This concept picture does not contain line numbers, but your result should.
* Generate static SVG image to show in the browser
    * You can use MathJax to convert Latex code to SVG
    * https://github.com/mathjax/MathJax-node
    * http://docs.mathjax.org/en/latest/tex.html
    * http://docs.mathjax.org/en/latest/options/output-processors/SVG.html
    * But the result should be a **static site without JavaScript** (use the server version)
